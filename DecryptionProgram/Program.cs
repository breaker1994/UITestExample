﻿using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Decrypter
{
    class Program
    {
        const string OutputForZip = "Encrypted";

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                string joinArgs = string.Join("\n", args),
                       regularPattern = @"^-u\n.+(\n-p\n.+)?\n.+(\n.+)?$",
                       sOutputDirectory = "Decrypted",
                       sFileName = null,
                       sUserName = null,
                       sPassword = null;
                Regex rRegex = new Regex(regularPattern);

                if (rRegex.IsMatch(joinArgs))
                {
                    sUserName = args[1];
                    switch (args.Length)
                    {
                        case 6:
                            sPassword = args[3];
                            sFileName = args[4];
                            sOutputDirectory = args[5];
                            break;
                        case 5:
                            sPassword = args[3];
                            sFileName = args[4];
                            break;
                        case 4:
                            sFileName = args[2];
                            sOutputDirectory = args[3];
                            break;
                        case 3:
                            sFileName = args[2];
                            break;
                    }
                    if (sPassword == null)
                    {
                        Console.Write("Password: ");
                        while (true)
                        {
                            var keyDown = Console.ReadKey(true);
                            if (keyDown.Key == ConsoleKey.Enter)
                            {
                                Console.WriteLine();
                                break;
                            }
                            sPassword += keyDown.KeyChar;
                        }
                    }
                    try
                    {
                        ZipFile.ExtractToDirectory(sFileName, OutputForZip);
                        Console.WriteLine("File '" + sFileName + "' was succesfully extracted!");
                    }
                    catch (FileNotFoundException err)
                    {
                        Console.WriteLine("File '" + sFileName + "' was not found!");
                        removeTemp();
                        Environment.Exit(1);
                    }

                    string Hash = generateSHA512Hash(sUserName + generateSHA512Hash(sPassword));

                    DirectoryInfo di;
                    if (!Directory.Exists(sOutputDirectory))
                    {
                        di = Directory.CreateDirectory(sOutputDirectory);
                        Console.WriteLine("Directory '"+ sOutputDirectory + "' was created");
                    }
                    DirectoryInfo workingDir = new DirectoryInfo(OutputForZip);
                    BinaryReader brArchiveReader;
                    byte[] bCryptoData, bSalt, bIV, bData;
                    foreach (var file in workingDir.EnumerateFiles("*", SearchOption.TopDirectoryOnly))
                    {
                        brArchiveReader = new BinaryReader(File.Open(file.FullName, FileMode.Open));
                        bCryptoData = brArchiveReader.ReadBytes((int)file.Length);
                        brArchiveReader.Close();

                        bSalt = new byte[16];
                        Array.Copy(bCryptoData, bCryptoData.Length - 32, bSalt, 0, 16);
                        bIV = new byte[16];
                        Array.Copy(bCryptoData, bCryptoData.Length - 16, bIV, 0, 16);
                        bData = new byte[bCryptoData.Length - 32];
                        Array.Copy(bCryptoData, bData, bData.Length);

                        bCryptoData = decryptAES256(Hash, bSalt, bIV, bData);

                        File.WriteAllBytes(sOutputDirectory+"/" + file.Name + ".jpg", bCryptoData);
                        Console.WriteLine("File '" + file.Name + ".jpg' was decrypted");
                    }
                    removeTemp();
                }
                else
                {
                    Console.WriteLine("Input is incorrect. Check syntax!");
                    getSyntax();
                }
            }
            else getSyntax();
        }
        static void getSyntax()
        {
            Console.WriteLine("Syntax: decrypter.exe -u userName [-p password] fileName [outputDir]\n\nParameters:");
            Console.WriteLine("     -u userName         User name.");
            Console.WriteLine("     -p userName         Password.");
            Console.WriteLine("     fileName            Name of the decrypting file.");
            Console.WriteLine("     outputDir           Output path.");
        }
        static void removeTemp()
        {
            DirectoryInfo di = new DirectoryInfo(OutputForZip);
            if(di.Exists)
            {
                foreach (FileInfo file in di.GetFiles()) file.Delete();
                di.Delete();
            }
        }
        static byte[] decryptAES256(String hash, byte[] salt, byte[] iv, byte[] data)
        {

            RijndaelManaged symmAES = new RijndaelManaged();
            symmAES.KeySize = 256;
            symmAES.Key = generatePBKDF2Hash(hash, salt);
            symmAES.IV = iv;
            symmAES.Mode = CipherMode.CBC;
            symmAES.Padding = PaddingMode.PKCS7;

            ICryptoTransform decryptor = symmAES.CreateDecryptor(symmAES.Key, symmAES.IV);
            byte[] bDecryptedBytes = null;
            try
            {
                bDecryptedBytes = decryptor.TransformFinalBlock(data, 0, data.Length);
            }
            catch(CryptographicException err)
            {
                Console.WriteLine("Incorrect login or password!");
                removeTemp();
                Environment.Exit(1);
            }
                        
            return bDecryptedBytes;
        }
        static byte[] generatePBKDF2Hash(String password, byte[] salt)
        {
            Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt);
            rfc2898DeriveBytes.IterationCount = 3000;
            return rfc2898DeriveBytes.GetBytes(32);
        }
        static String generateSHA512Hash(String Input)
        {
            SHA512Managed sha5 = new SHA512Managed();
            byte[] ba2 = sha5.ComputeHash(Encoding.UTF8.GetBytes(Input));

            StringBuilder hex = new StringBuilder(ba2.Length * 2);
            foreach (byte b in ba2) hex.AppendFormat("{0:x2}", b);

            return hex.ToString();
        }
    }
}