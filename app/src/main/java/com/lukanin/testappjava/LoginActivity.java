package com.lukanin.testappjava;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class LoginActivity extends Activity {
    static final String NAME_HASH_FILE = "/.hash",
            NAME_HASH_EXTRA = "Hash";

    EditText edtLogin, edtPassword;
    Button btnLogin, btnExit;
    File fileHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        fileHash = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name) + NAME_HASH_FILE);

        edtLogin = (EditText) findViewById(R.id.edtLogin);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        btnExit = (Button) findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileHash = null;
                System.exit(0);
            }
        });

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtLogin.length() == 0 || edtPassword.length() == 0) {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_empty_fields), Toast.LENGTH_LONG).show();
                } else {
                    btnLogin.setEnabled(false);
                    try {
                        if (edtPassword.length() > 7 && BCrypt.checkpw(edtLogin.getText().toString() + edtPassword.getText().toString(), new Scanner(fileHash).next())) {
                            Intent intentCameraActivity = new Intent(LoginActivity.this, CameraActivity.class);
                            intentCameraActivity.putExtra(NAME_HASH_EXTRA, Crypto.generateSHA512Hash(edtLogin.getText().toString() + Crypto.generateSHA512Hash(edtPassword.getText().toString())));
                            intentCameraActivity.putExtra("Login", edtLogin.getText().toString());
                            startActivity(intentCameraActivity);
                            finish();
                        } else {
                            btnLogin.setEnabled(true);
                            Toast.makeText(getApplicationContext(), getString(R.string.error_incorrect_fields), Toast.LENGTH_LONG).show();
                        }
                    } catch (FileNotFoundException err) {
                        btnLogin.setEnabled(true);

                        err.printStackTrace();
                        Toast.makeText(getApplicationContext(), getString(R.string.error_hashfile_corrupted), Toast.LENGTH_LONG).show();
                    } catch (NoSuchAlgorithmException err) {
                        err.printStackTrace();
                        System.exit(1);
                    }
                }
            }
        });
    }
}