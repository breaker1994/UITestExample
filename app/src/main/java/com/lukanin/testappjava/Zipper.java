package com.lukanin.testappjava;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class Zipper {
    static final String NAME_TEMP_ARCHIVE = "Temp.zip",
            NAME_FINAL_ARCHIVE = "Result.zip";

    public String archivePath;

    public Zipper(String path) {
        archivePath = path;
    }

    public void addEntryToZip(String fileName, byte[] data) {
        ZipFile originalZipFile = null;
        FileOutputStream foZipArchive = null;
        ZipOutputStream zoZipArchive = null;

        Boolean existingOfOriginalArchive = true;
        try {
            originalZipFile = new ZipFile(archivePath + NAME_FINAL_ARCHIVE);
        } catch (IOException err) {
            existingOfOriginalArchive = false;
        }
        try {
            foZipArchive = new FileOutputStream(archivePath + (existingOfOriginalArchive ? NAME_TEMP_ARCHIVE : NAME_FINAL_ARCHIVE));
            zoZipArchive = new ZipOutputStream(foZipArchive);

            if (existingOfOriginalArchive) {
                Enumeration<? extends ZipEntry> existingEntries = originalZipFile.entries();
                while (existingEntries.hasMoreElements()) {
                    ZipEntry oneSingleEntry = existingEntries.nextElement();
                    zoZipArchive.putNextEntry(oneSingleEntry);
                    copyData(originalZipFile.getInputStream(oneSingleEntry), zoZipArchive);
                    zoZipArchive.closeEntry();
                }
            }

            ZipEntry someNewStuff = new ZipEntry(fileName);
            zoZipArchive.putNextEntry(someNewStuff);
            zoZipArchive.write(data);
            zoZipArchive.closeEntry();
        } catch (IOException err) {
        } finally {
            if (existingOfOriginalArchive) {
                try {
                    originalZipFile.close();
                } catch (IOException |
                        NullPointerException err) {
                }
                File originalArchiveFile = new File(archivePath + NAME_FINAL_ARCHIVE);
                File newArchiveFile = new File(archivePath + NAME_TEMP_ARCHIVE);
                originalArchiveFile.delete();
                newArchiveFile.renameTo(originalArchiveFile);
            }
            try {
                zoZipArchive.close();
            } catch (IOException |
                    NullPointerException err) {
            }
            try {
                foZipArchive.close();
            } catch (IOException |
                    NullPointerException err) {
            }
        }
    }

    private static void copyData(InputStream input, OutputStream output) throws IOException {
        int bytesRead;
        byte[] bytesBuffer = new byte[1024];

        while ((bytesRead = input.read(bytesBuffer)) != -1)
            output.write(bytesBuffer, 0, bytesRead);
    }
}