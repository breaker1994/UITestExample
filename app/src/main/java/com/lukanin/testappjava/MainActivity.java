package com.lukanin.testappjava;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Bundle;
import android.Manifest;

import java.io.File;

public class MainActivity extends Activity
{
    static final String NAME_HASH_FILE = "/.hash";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123; //Код для принятого разрешения

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT > 22 && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);
        } else onPermissionSuccess();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // После получения разрешения из группы Storage нам необходимо перезагрузить приложение, ибо без этого система будет думать, что у нас все еще этого разрешения нет! >_<

                PendingIntent pi = PendingIntent.getActivity(this, 0, getIntent(), PendingIntent.FLAG_CANCEL_CURRENT);

                AlarmManager amRestart = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                amRestart.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, pi);

                System.exit(0);
            } else System.exit(0);
        }
    }
    public void onPermissionSuccess() {
        String strTemp = getString(R.string.app_name);

        File fileCheckingExistFile = new File(Environment.getExternalStorageDirectory(), strTemp);
        if (!fileCheckingExistFile.exists()) fileCheckingExistFile.mkdirs();

        fileCheckingExistFile = new File(Environment.getExternalStorageDirectory(), strTemp + NAME_HASH_FILE);
        Intent intentNewActivity = new Intent(this, fileCheckingExistFile.exists() ? LoginActivity.class : RegisterActivity.class);

        startActivity(intentNewActivity);
        finish();
    }
}