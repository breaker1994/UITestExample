package com.lukanin.testappjava;

import com.google.common.primitives.Bytes;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

public class Crypto {
    public static String generateSHA512Hash(String Input) throws NoSuchAlgorithmException {
        MessageDigest msdgTemp = MessageDigest.getInstance("SHA-512");
        msdgTemp.update(Input.getBytes());
        return toHex(msdgTemp.digest());
    }

    public static String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger biTemp = new BigInteger(1, array);
        String hex = biTemp.toString(16);

        int iPaddingLength = (array.length * 2) - hex.length();
        if (iPaddingLength > 0) return String.format("%0" + iPaddingLength + "d", 0) + hex;
        else return hex;
    }

    public static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom srTemp = SecureRandom.getInstance("SHA1PRNG");
        byte[] btSalt = new byte[16];
        srTemp.nextBytes(btSalt);
        return btSalt;
    }

    public static byte[] generatePBKDF2Hash(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PBEKeySpec keyspTemp = new PBEKeySpec(password.toCharArray(), salt, 3000, 256);
        SecretKeyFactory skfTemp = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] btHash = skfTemp.generateSecret(keyspTemp).getEncoded();
        return btHash;
    }

    public static byte[] encryptAES256(String hash, byte[] data) throws
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeySpecException,
            InvalidKeyException,
            BadPaddingException,
            UnsupportedEncodingException,
            IllegalBlockSizeException,
            InvalidParameterSpecException,
            InvalidAlgorithmParameterException {
        byte[] btSalt = Crypto.getSalt();

        SecretKeySpec keyspSecret = new SecretKeySpec(generatePBKDF2Hash(hash, btSalt), "AES");
        Cipher cipTemp = Cipher.getInstance("AES/CBC/PKCS7Padding");
        cipTemp.init(Cipher.ENCRYPT_MODE, keyspSecret);
        AlgorithmParameters apTempParams = cipTemp.getParameters();

        return Bytes.concat(cipTemp.doFinal(data), // Шифрованные данные
                btSalt, // 16 байт, соль для PBKDF2
                apTempParams.getParameterSpec(IvParameterSpec.class).getIV()); // 16 байт, вектор инициализации для AES256
    }

    /* Для дешифровки

    public static byte[] decryptAES256(String hash, byte[] salt, byte[] iv, byte[] data) throws
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeySpecException,
            InvalidKeyException,
            BadPaddingException,
            UnsupportedEncodingException,
            IllegalBlockSizeException,
            InvalidAlgorithmParameterException
    {
        SecretKeySpec keyspSecret = new SecretKeySpec(generatePBKDF2Hash(hash, salt), "AES");
        Cipher cipTemp = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipTemp.init(Cipher.DECRYPT_MODE, keyspSecret, new IvParameterSpec(iv));
        return cipTemp.doFinal(data);
    }*/
}