package com.lukanin.testappjava;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;

public class RegisterActivity extends Activity {
    static final String NAME_HASH_FILE = "/.hash",
            NAME_HASH_EXTRA = "Hash";

    EditText edtLogin, edtPassword, edtRepeatPassword;
    Button btnLogin, btnExit;
    File fileHash;
    Spinner genderSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fileHash = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name) + NAME_HASH_FILE);

        setContentView(R.layout.activity_registration);

        genderSpinner = (Spinner) findViewById(R.id.genderSpinner);

        edtLogin = (EditText) findViewById(R.id.edtLogin);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtRepeatPassword = (EditText) findViewById(R.id.edtRepeatPassword);

        btnExit = (Button) findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileHash = null;
                System.exit(0);
            }
        });

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtLogin.length() == 0 || edtPassword.length() == 0 || edtRepeatPassword.length() == 0) {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_empty_fields), Toast.LENGTH_LONG).show();
                } else if (edtRepeatPassword.length() < 8 || edtPassword.length() < 8) {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_short_fields), Toast.LENGTH_LONG).show();
                } else if (!edtRepeatPassword.getText().toString().equals(edtPassword.getText().toString())) {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_different_passwords), Toast.LENGTH_LONG).show();
                } else {
                    btnLogin.setEnabled(false);
                    PrintWriter pwrOutHash = null;
                    try {
                        fileHash.createNewFile();
                        pwrOutHash = new PrintWriter(fileHash);
                        pwrOutHash.write(BCrypt.hashpw(edtLogin.getText().toString() + edtPassword.getText().toString(), BCrypt.gensalt(12)));
                        pwrOutHash.close();
                        Intent intentCameraActivity = new Intent(RegisterActivity.this, CameraActivity.class);
                        intentCameraActivity.putExtra(NAME_HASH_EXTRA, Crypto.generateSHA512Hash(edtLogin.getText().toString() + Crypto.generateSHA512Hash(edtPassword.getText().toString())));
                        intentCameraActivity.putExtra("Login", edtLogin.getText().toString());
                        intentCameraActivity.putExtra("Gender", genderSpinner.getSelectedItem().toString());
                        startActivity(intentCameraActivity);
                        finish();
                    } catch (IOException err) {
                        btnLogin.setEnabled(true);

                        err.printStackTrace();
                        Toast.makeText(getApplicationContext(), err.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (NoSuchAlgorithmException err) {
                        err.printStackTrace();
                        System.exit(1);
                    } finally {
                        if (pwrOutHash != null) pwrOutHash.close();
                    }
                }
            }
        });
    }
}