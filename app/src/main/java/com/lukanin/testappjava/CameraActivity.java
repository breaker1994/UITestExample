package com.lukanin.testappjava;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.util.DisplayMetrics;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.List;

public class CameraActivity extends Activity {
    static final String NAME_HASH_EXTRA = "Hash",
            NAME_FILE_PREFIX = "PH";

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123; //Код для принятого разрешения (permission)

    Button btnExit, btnPhoto;
    HolderCallback clbForPhoto;
    SurfaceHolder hldForPhoto;
    SurfaceView sfvPhotoPlace;
    Camera cmrCamera;
    TextView edtLoginGreet;
    boolean boolPreviewIsRunning,
            boolIsPermissionGranted;
    Zipper zipArchive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT > 22 && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_ASK_PERMISSIONS);
        } else onPermissionSuccess();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                onPermissionSuccess();
            else {
                Toast.makeText(getApplicationContext(), getString(R.string.error_permission_denied), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onPermissionSuccess() {
        boolIsPermissionGranted = true;

        zipArchive = new Zipper(Environment.getExternalStorageDirectory()
                + File.separator
                + getString(R.string.app_name)
                + File.separator);

        setContentView(R.layout.activity_camera);

        Intent intent = getIntent();
        String greetText = "Hi, "+intent.getStringExtra("Login");
        if(intent.hasExtra("Gender") && !intent.getStringExtra("Gender").equals("None")) {
            greetText += " ("+intent.getStringExtra("Gender")+")";
        }
        greetText += "!";

        edtLoginGreet = (TextView) findViewById(R.id.loginGreetings);
        edtLoginGreet.setText(greetText);

        btnExit = (Button) findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cmrCamera != null && boolPreviewIsRunning) ownReleaseCamera();
                finish();
            }
        });
        btnPhoto = (Button) findViewById(R.id.btnPhoto);
        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnPhoto.setEnabled(false);
                boolPreviewIsRunning = false;
                cmrCamera.takePicture(null, null, null, new PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] btTakenPhoto, Camera cmrCamera) {
                        if (btTakenPhoto != null) {
                            try {
                                long lngUnixTime = System.currentTimeMillis() / 1000L; // Для генерации названий файлов

                                Intent intent = getIntent();
                                Log.d("Hash", intent.getStringExtra(NAME_HASH_EXTRA));
                                zipArchive.addEntryToZip(NAME_FILE_PREFIX + lngUnixTime,
                                        Crypto.encryptAES256(intent.getStringExtra(NAME_HASH_EXTRA), btTakenPhoto));

                                Toast.makeText(getApplicationContext(), getString(R.string.name_photo_created) + " " + NAME_FILE_PREFIX + lngUnixTime, Toast.LENGTH_LONG).show();
                            } catch (IllegalBlockSizeException |
                                    NoSuchAlgorithmException |
                                    NoSuchPaddingException |
                                    InvalidKeyException |
                                    InvalidKeySpecException |
                                    BadPaddingException |
                                    InvalidParameterSpecException |
                                    InvalidAlgorithmParameterException |
                                    UnsupportedEncodingException err) {
                                err.printStackTrace();
                                System.exit(1);
                            }
                        }
                        ownStartPreview();
                        btnPhoto.setEnabled(true);
                    }
                });
            }
        });

        sfvPhotoPlace = (SurfaceView) findViewById(R.id.PhotoPlace);
        hldForPhoto = sfvPhotoPlace.getHolder();

        clbForPhoto = new HolderCallback();
        hldForPhoto.addCallback(clbForPhoto);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (boolIsPermissionGranted) {
            ownReleaseCamera();
            finish();
        }
    }

    class HolderCallback implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder hldHolder) {
            int iCameraID = -1;
            CameraInfo infCameraInfo = new CameraInfo();

            for (int i = 0; i < Camera.getNumberOfCameras(); ++i) {
                Camera.getCameraInfo(i, infCameraInfo);
                if (infCameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                    iCameraID = i;
                    break;
                }
            }
            if (iCameraID != -1) {
                try {
                    cmrCamera = Camera.open(iCameraID);
                    cmrCamera.setDisplayOrientation(90);
                    Parameters prmParameters = cmrCamera.getParameters();

                    List<String> lstFlashModes = prmParameters.getSupportedFlashModes();
                    if (lstFlashModes != null && lstFlashModes.contains(Parameters.FLASH_MODE_OFF)) {
                        prmParameters.setFlashMode(Parameters.FLASH_MODE_OFF); //Без вспышки
                    }

                    List<String> lstFocusModes = prmParameters.getSupportedFocusModes();
                    if (lstFocusModes != null && lstFocusModes.contains(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                        prmParameters.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE); //Фокусировка происходит непрерывно
                    }

                    List<Size> lstSizes = prmParameters.getSupportedPictureSizes();
                    if (lstSizes != null && lstSizes.size() > 0) {
                        Size szPictureSize = lstSizes.get(0);
                        prmParameters.setPictureSize(szPictureSize.width, szPictureSize.height);
                    }

                    List<Size> lstPreviewSizes = prmParameters.getSupportedPreviewSizes();
                    DisplayMetrics metrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(metrics);

                    if (lstPreviewSizes != null && lstPreviewSizes.size() > 0) {
                        Size szPreviewSize = getOptimalPreviewSize(lstPreviewSizes, metrics.heightPixels);
                        prmParameters.setPreviewSize(szPreviewSize.width, szPreviewSize.height);
                    }

                    cmrCamera.setParameters(prmParameters);
                    cmrCamera.enableShutterSound(false); //Выключаем звук затвора камеры
                } catch (Exception err) {
                    err.printStackTrace();
                    System.exit(1);
                }
            } else return;
            try {
                cmrCamera.setPreviewDisplay(hldHolder);
            } catch (Exception err) {
                err.printStackTrace();
                System.exit(1);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder hldHolder, int iFormat, int iWidth, int iHeight) {
            ownStartPreview();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder hldHolder) {
            ownReleaseCamera();
        }
    }

    private Size getOptimalPreviewSize(List<Size> lstSizes, int iHeight) {
        int iMinDiff = 0, iTempHeight;
        Size szOptimalPreviewSize = null;

        for (Size sCurSize : lstSizes) {
            iTempHeight = sCurSize.height > sCurSize.width ? sCurSize.height : sCurSize.width;
            if (iTempHeight <= iHeight && iTempHeight > iMinDiff) {
                szOptimalPreviewSize = sCurSize;
                iMinDiff = iTempHeight;
            }
        }
        return szOptimalPreviewSize;
    }

    private void ownReleaseCamera() {
        if (cmrCamera != null) {
            cmrCamera.setPreviewCallback(null);
            cmrCamera.stopPreview();
            cmrCamera.release();
            cmrCamera = null;
        }
    }

    private void ownStartPreview() {
        if (!boolPreviewIsRunning && cmrCamera != null) {
            cmrCamera.startPreview();
            boolPreviewIsRunning = true;
        }
    }
}