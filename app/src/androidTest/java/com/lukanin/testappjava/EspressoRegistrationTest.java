package com.lukanin.testappjava;

import android.Manifest;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

@RunWith(AndroidJUnit4.class)
public class EspressoRegistrationTest {
    @Rule
    public GrantPermissionRule writePermissionRule = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Rule
    public final ActivityTestRule<RegisterActivity> registerActivity = new ActivityTestRule<>(RegisterActivity.class);

    @Test
    public void testDifferentPasswords() throws Exception {
        Espresso.onView(withId(R.id.edtLogin)).perform(typeText("login"));
        Espresso.onView(withId(R.id.edtPassword)).perform(typeText("password1"));
        Espresso.onView(withId(R.id.edtRepeatPassword)).perform(typeText("password2"));
        Espresso.onView(withId(R.id.btnLogin)).perform(click());
        Espresso.onView(withId(R.id.PhotoPlace)).check(doesNotExist());
    }

    @Test
    public void testSamePasswords() throws Exception {
        Espresso.onView(withId(R.id.edtLogin)).perform(typeText("login"));
        Espresso.onView(withId(R.id.edtPassword)).perform(typeText("password"));
        Espresso.onView(withId(R.id.edtRepeatPassword)).perform(typeText("password"));
        Espresso.onView(withId(R.id.btnLogin)).perform(click());
        Espresso.onView(withId(R.id.PhotoPlace)).check(matches(isDisplayed()));
    }

    @Test
    public void testLoginGreeting() throws Exception {
        Espresso.onView(withId(R.id.edtLogin)).perform(typeText("login"));
        Espresso.onView(withId(R.id.edtPassword)).perform(typeText("password"));
        Espresso.onView(withId(R.id.edtRepeatPassword)).perform(typeText("password"));

        Espresso.onView(withId(R.id.btnLogin)).perform(click());

        String neededResult = "Hi, login!";
        Espresso.onView(withId(R.id.loginGreetings)).check(matches(withText(neededResult)));
    }

    @Test
    public void testSpinner() throws Exception {
        Espresso.onView(withId(R.id.edtLogin)).perform(typeText("login"));
        Espresso.onView(withId(R.id.edtPassword)).perform(typeText("password"));
        Espresso.onView(withId(R.id.edtRepeatPassword)).perform(typeText("password"));

        Espresso.onView(withId(R.id.genderSpinner)).perform(click());
        Espresso.onData(allOf(is(instanceOf(String.class)), is("Male"))).perform(click());
        Espresso.onView(withId(R.id.genderSpinner)).check(matches(withSpinnerText("Male")));

        Espresso.onView(withId(R.id.btnLogin)).perform(click());

        String neededResult = "Hi, login (Male)!";
        Espresso.onView(withId(R.id.loginGreetings)).check(matches(withText(neededResult)));
    }
}
